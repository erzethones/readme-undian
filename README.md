# Aplikasi undian

Aplikasi undian tabungan menggunakan database sqlite untuk portabilitas.
Menggunakan Clean Architecture <https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html>
sebagai dasar desain struktur program.


## Instalasi dan Penggunaan Aplikasi

Untuk menjalankan aplikasi, direkomendasikan sudah terinstall aplikasi sebagai
berikut:

	1. PHP versi 7 dengan pdo_sqlite terinstall(akan muncul error jika pdo_sqlite tidak ada).

Langkah pertama, anda diharuskan untuk mendownload 'composer' dan mengarahkan penyimpanan download nya pada folder project anda

	https://getcomposer.org/download/1.8.6/composer.phar

Silahkan Mendownload dari link di atas.

Next, jalankan operasi berikut untuk mendownload dependencies:
	
	php composer.phar install

Atau anda bisa menjalankan / double click pada file : ZZZ_Install_Dependencies.bat

Untuk menjalankan aplikasi bisa menggunakan:

	php composer.phar start

atau:

	php composer.phar serve

atau:

	double click pada file : ZZZ_Start_Server.bat

Sedangkan untuk menjalankan test suite menggunakan command (you can skip this step):

	php composer.phar test

untuk membuka aplikasi melalui

	http://localhost:8080

Alur aplikasi:
1. Buat golongan undian
2. Upload data undian
3. Posting undian

Untuk data import:
- format harus CSV
- Kolom yang harus disertakan:
	1. Nomor_Undian,
	2. Rekening,
	3. cif,
	4. Nama,
	5. Alamat
- Urutan kolom boleh berbeda.